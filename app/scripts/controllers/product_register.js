'use strict';

/**
 * @ngdoc function
 * @name ammwayApp.controller:ProductRegisterCtrl
 * @description
 * # ProductRegisterCtrl
 * Controller of the ammwayApp
 */
angular.module('ammwayApp')
    .controller('ProductRegisterCtrl', function($scope) {
        this.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];
        $scope.database = firebase.database();
        $scope.productCatalog = $scope.database.ref("/products")
        $scope.productCatalog.on('value', function(snapshot) {
            console.log(snapshot.val())
        });
        $scope.newProduct = {}
        $scope.newProduct.category = "NUTRILITE";
        $scope.setSelection = function(selectedOption){
          $scope.selectedCategory = selectedOption.target.innerHTML;
          $scope.newProduct.category = $scope.selectedCategory;
          $(".dd-text").text($scope.selectedCategory)
        }

        $scope.submitProduct = function() {
            $scope.newProduct.points = parseInt($scope.newProduct.points)
            $scope.newProduct.vn = parseInt($scope.newProduct.vn)
            $scope.newProduct.publicPrice = parseInt($scope.newProduct.publicPrice)
            $scope.newProduct.price25 = $scope.newProduct.publicPrice * .75;
            $scope.newProduct.price30 = $scope.newProduct.publicPrice * .70;
            console.log($scope.newProduct)
            $scope.productCatalog.push($scope.newProduct)
            $scope.newProduct = {}
        }
    });