'use strict';

/**
 * @ngdoc function
 * @name ammwayApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ammwayApp
 */
angular.module('ammwayApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
