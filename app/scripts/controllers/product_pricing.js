'use strict';

/**
 * @ngdoc function
 * @name ammwayApp.controller:ProductPricingCtrl
 * @description
 * # ProductPricingCtrl
 * Controller of the ammwayApp
 */
angular.module('ammwayApp')
    .controller('ProductPricingCtrl', function($scope) {
        this.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];
        $scope.database = firebase.database();
        $scope.productCatalog = $scope.database.ref("/products")
        $scope.prodsList;
        $scope.productsToFilter = [];
        $scope.cartList = [];
        $scope.productCatalog.on('value', function(snapshot) {
            console.log(snapshot.val())
            $scope.prodsList = snapshot.val()
            var log = [];
            angular.forEach($scope.prodsList, function(value, key) {
                $scope.productsToFilter.push(value);
            }, log);
            $scope.$apply();
            console.log($scope.productsToFilter)
        });
        $scope.addToCart = function(obj,rowIndex) {
          console.log(rowIndex)
          $scope.productIndex = parseInt(obj.target.attributes["data-object-index"].value)
          console.log($scope.productIndex)
            $scope.addedProduct = $scope.productsToFilter[$scope.productIndex];
            console.log($scope.addedProduct)
            $scope.addedQuantity = parseFloat($(".products-list").find("tr:eq(" + rowIndex + ")").find(".product-quantity").val());
            console.log($scope.addedQuantity)
            $scope.addedProduct.quantity = $scope.addedQuantity
            //$scope.addedProduct.totalPrice = $scope.addedProduct.quantity * $scope.addedProduct.publicPrice
            $scope.cartList.push($scope.productsToFilter[$scope.productIndex])
            $scope.productsToFilter.splice($scope.productIndex, 1)
            console.log($scope.totalPricing)
        }
        $scope.removeFromCart = function(obj, productIndex) {
            $scope.removedProduct = $scope.cartList[productIndex]
            $scope.productsToFilter.push($scope.removedProduct)
            $scope.cartList.splice(productIndex, 1)
        }
    });
