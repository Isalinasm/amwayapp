'use strict';

/**
 * @ngdoc function
 * @name ammwayApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ammwayApp
 */
angular.module('ammwayApp')
  .controller('MainCtrl', function ($scope) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.database = firebase.database();
    $scope.productCatalog = $scope.database.ref("/products")
    $scope.productCatalog.on('value', function(snapshot) {
  		console.log(snapshot.val())
  		//updateStarCount(postElement, snapshot.val());
	});

  });
