'use strict';

/**
 * @ngdoc overview
 * @name ammwayApp
 * @description
 * # ammwayApp
 *
 * Main module of the application.
 */
angular
  .module('ammwayApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {/*
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'*/
        /*templateUrl: 'views/product_register.html',
        controller: 'ProductRegisterCtrl',
        controllerAs: 'productRegister'*/
        templateUrl: 'views/product_pricing.html',
        controller: 'ProductPricingCtrl',
        controllerAs: 'productPricing'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/product_register', {
        templateUrl: 'views/product_register.html',
        controller: 'ProductRegisterCtrl',
        controllerAs: 'productRegister'
      })
      .when('/product_pricing', {
        templateUrl: 'views/product_pricing.html',
        controller: 'ProductPricingCtrl',
        controllerAs: 'productPricing'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
