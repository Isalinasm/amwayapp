'use strict';

describe('Controller: ProductRegisterCtrl', function () {

  // load the controller's module
  beforeEach(module('ammwayApp'));

  var ProductRegisterCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProductRegisterCtrl = $controller('ProductRegisterCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ProductRegisterCtrl.awesomeThings.length).toBe(3);
  });
});
