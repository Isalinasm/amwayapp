'use strict';

describe('Controller: ProductPricingCtrl', function () {

  // load the controller's module
  beforeEach(module('ammwayApp'));

  var ProductPricingCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProductPricingCtrl = $controller('ProductPricingCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ProductPricingCtrl.awesomeThings.length).toBe(3);
  });
});
